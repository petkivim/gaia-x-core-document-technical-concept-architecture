ADR-000: Interconnections as a Dedicated Asset Category
=======================================================

:adr-id: 000
:revnumber: 1.0
:revdate: 23-12-2020
:status: draft
:author: 
:stakeholder: 

Summary
-------

Interconnections are represented by a dedicated category of Assets in GAIA-X.

Context
-------

For some use-cases in GAIA-X it is important to make the communication channels
explicit. For example when data must remain in a certain geographic region (also
when the data is in transit) and so that access to communication capacity can be
handled as a marketable Asset.

Handling interconnection as either a Service or special type of Node is not
possible, as Interconnections have properties that are fulfilled by neither
Asset category. Therefore a dedicated Asset category for Interconnections is
proposed.

It is however possible to provide "Interconnection Services" that tie together
one or several Interconnections and Nodes.

Decision Statements
-------------------

An interconnection constitutes a dedicated category of a `Resource` in GAIA-X and is a connection between two or multiple nodes.
These nodes are usually deployed in different infrastructure domains and owned by different stakeholders, such as customers and/or providers.
The interconnection between the nodes can be seen as a path, which exhibits special characteristics, such as latency and bandwidth guarantees, that go beyond the characterstics of a path over the public Internet.

Interconnections describe communication infrastructure that exists in the
physical world. Software-defined (virtual, configured) network topologies on top
of the physical infrastructure are not considered.

Each Interconnection is operated by a single Provider.

Several Nodes can be connected to the same Interconnection, indicating that these
Nodes can communicate.

Interconnections can be connected among each other without another Node "in between".

Interconnections, Nodes and their connections form a graph that may also contain
cycles.

Contrary to Nodes, Interconnections cannot be nested in a parent-child relation.

A nested Node (e.g. a data-center containing servers that are themselves Nodes)
can contain Interconnections (how the servers in the data-center are connected).
The parent Node can be connected to the contained Interconnection. But no "outside
Node" can connect to an "inside Interconnection".

Consequences
------------

Communication channels and the Providers operating them can be represented
explicitly in the Self-Descriptions.

"Interconnection Services" can describe which underlying communication
infrastructure they (may) use.

ADR References
--------------

External References
-------------------
