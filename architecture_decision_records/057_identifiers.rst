ADR-XXX: GAIA-X Identifier Format
==============================================

:adr-id: XXX
:revnumber: 2.1
:revdate: 2021-03-19
:status: proposed
:author: GAIA-X Catalogue and IAM Community
:stakeholder: IAM WG, Self-Description WG, Catalogue WG

Summary
-------

ADR-001 defines the use of JSON-LD for the Self-Descriptions. JSON-LD requires
that Identifiers used for cross-referencing between self-descriptions are IRIs
(Internationalized Resource Identifiers [RFC3987]). This ADR upholds this
definition and further refines it.

Identifiers used in GAIA-X shall be URIs following the [RFC3986].

Identifier must re-use existing schemas or define their own private URI
schema(s) eu.gaia-x.<protocol> where necessary [BCP35]. The schema shall define
additional semantics to indicate the underlying protocol.

Context
-------

The generic structure of the identifier takes the form:

  <schema>:<protocol_specific_id>

For protocols requiring a new URI schema a private schema should be defined
following the pattern:

  eu.gaia-x.<protocol>

The following Identifier schemas have a defined mechanism to ensure uniqueness
of the Identifier. More schemas may be added in the future.

    Protocol         Schema              Protocol_Specific_Id
    -----------------------------------------------------------------
    TAG [RFC4151]    urn:tag             <domain>,<time>:<identifier>
    OpenID Connect   eu.gaia-x.openid    <iss>;<sub>
    DID              did                 <method>:<identifier> 

Tag URI Schema
~~~~~~~~~~~~~~

Identifiers used in Self-Descriptions may follow the conventions of RFC 4151 for
the 'tag' URI scheme. Identifiers of this format contain the DNS domain name or
an email of the issuing organization as well as a date at which the organization
was in possession of the DNS domain. That way, the organization in possession of
the DNS domain at that time is responsible to issue only unique Identifiers.

Some examples of Identifiers::

    urn:tag:provider-name.com,2020:my-service:v1
    urn:tag:subdomain.foobar.com,2020-01:org1/data-asset5/element20
    urn:tag:foobar@acme.org,2020-01-29:e51a9f18273718445f0c016f23b2bc05919f7433

By the convention that only the organization owning the domain-name may use it
for Identifiers, GAIA-X Participants can themselves issue new Identifiers and
ensure that Identifiers are unique without the need for a central identifier
registry for all GAIA-X Participants.

OpenID Connect URI Schema
~~~~~~~~~~~~~~~~~~~~~~~~~

OpenID Connect   eu.gaia-x.openid    <iss>;<sub>

Example:  
eu.gaia-x.openid:https://example-idp.org/auth/realms/master;YWxpY2VAZm9vLmNvbQ

Companies have to host an endpoint that is part of the Identifier.
To ensure uniqueness, endpoints might need to change after a domain changes ownership and uniqueness of identifiers cannot be otherwise guaranteed.


DID URI Schema
~~~~~~~~~~~~~~

DID identifiers according to W3C "Decentralized Identifiers", Candidate
Recommendation: https://www.w3.org/TR/did-core refer to a "method".

1. The method refers to a "Verifiable Data Registry" where the DID can be
   resolved to a document.
2. The Verifiable Data Registry ensures uniqueness of the Identifiers.

Examples for such Verifiable Data registries include "distributed ledgers,
decentralized file systems, databases of any kind, peer-to-peer networks, and
other forms of trusted data storage" as described in
https://www.w3.org/TR/did-core/#architecture-overview

Decision Statements
-------------------

GAIA-X Identifiers uniquely identify an entity in GAIA-X.
Informational: Entities can refer to (non-exhaustive)
- Entities with a Self-Description (contains Participant)
- Principals (user accounts)
- Abstract Concepts (for example "European Economic Area" or "ISO 27001").

GAIA-X Identifiers are unique in the sense that an Identifier must never refer
to more than one entity. There can be several GAIA-X Identifiers refering to the
same entity.

Informational: As a policy, multiple Identifiers for the same entity should be
avoided in the Catalogue.

All Identifiers used in GAIA-X are URIs following the [RFC3986] specification.

Informational: JSON-LD allows IRIs. URIs are a strict subset of that.

The lifetime of an Identifier is permanent. That is, the Identifier has to be
unique forever, and may be used as a reference to an entity well beyond the
lifetime of the entity it identifies or of any naming authority involved in the
assignment of its name [RFC1737]. Reuse of an Identifier for a different entity,
also at a later time, is forbidden.

There are multiple valid URI schemas defined, each associated with a technical
mechanism to ensure uniquenes. The structure of an identifier has to ensure the
uniqueness of the Identifier.

Informational:
GAIA-X Participants can self-issue Identifiers. It is solely the responsibility
of a Participant to determine the conditions under which the Identifier will be
issued. A self-issued Identifier can be used without publicly registering or
announcing the Identifier first.
Not all URI schemas are usable for self-issuing.

Informational:
Identifiers shall be derived from the native identifiers of an Identity System
without any separate attribute needed. The Identifier shall provide a clear
reference to the Identity System technology used. OpenID Connect and DID shall
be supported. Any scheme for Identifiers must permit future extensions to the
scheme.

Informational:
The Identifier shall be comparable in the raw form. It shall not be needed to
make any transformation to compare two identifiers and tell whether they are the
same.

Informational:
Identifiers should not contain more information than necessary (including
Personal Identifiable Information).


Consequences
------------

GAIA-X Participants Identity Systems can self-issue valid Principal Identifiers.

Based on the identifier it is possible to determine the technology and the unique reference
to the Identity.


ADR References
--------------

* ADR-001

External References
-------------------

* [BCP35] Guidelines and Registration Procedures for URI Schemes. https://tools.ietf.org/html/bcp35
* [DID-Core] Decentralized Identifiers (DIDs) v1.0. https://www.w3.org/TR/did-core/
* [IAM Framework] GAIA-X IAM Framework v1.01. https://docs.google.com/document/d/1XCjIVRul_w_6runDn_Rh-8nVdMhSFmMxZTXoQAhtISA/
* [RFC1737] Functional Requirements for Uniform Resource Names. https://tools.ietf.org/html/rfc1737
* [RFC3986] Uniform Resource Identifier (URI): Generic Syntax. https://tools.ietf.org/html/rfc3986
