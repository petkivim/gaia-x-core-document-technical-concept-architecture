GAIA-X Architecture Decision Records
====================================

Architecture Decision Records (ADR) document important decisions. This ensures
the synchronization between Work Packages. This is especially important as new
members join GAIA-X.

Accepted ADR are appended to the Architecture Document. Future releases of the
Architecture Document state up to which ADR changes have been incorporated.

ADR are written in the RestructuredText format. This is both human-readable, fit
for source versioning and can be translated into most other document format. The
`Primer on Restructured Text
<https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_
explains the formatting rules. See the file 000_adr_template.rst for an example.

Current ADR
-----------

+-------------------------------------------------------------------+------------+--------------+
| ADR                                                               | Status     | Last Changed |
+===================================================================+============+==============+
| ADR-001: JSON-LD as the Exchange Format for Self-Descriptions     | accepted   | 24 Jul, 2020 |
+-------------------------------------------------------------------+------------+--------------+
| ADR-002: REST as the Interface Technology for Federation Services | accepted   | 28 Aug, 2020 |
+-------------------------------------------------------------------+------------+--------------+
| ADR-XXX: Identifiers used in Self-Descriptions                    | proposed   | 05 Nov, 2020 |
+-------------------------------------------------------------------+------------+--------------+
| ADR-XXX: Lifecycle of Self-Descriptions                           | proposed   | 05 Nov, 2020 |
+-------------------------------------------------------------------+------------+--------------+

ADR Lifecycle
-------------

draft
   The ADR is still work in progress and has not been proposed by a Work Package
   or several work packages.

proposed
   The ADR has been proposed, but there is no final decision on it so far. A
   proposed ADR is uploaded to the repository and communicated within the
   community.

rejects
   The ADR has been rejected.

accepted
   The ADR has been accepted.

deprecated
   The ADR was once accepted but is no longer effective.

superseded
   The ADR was once accepted. But the content has been superseded by a more
   recent ADR.

Process
-------

`See this overview diagramm. <2020-08-05_ADR-Gaia-X-process.png>`_

FAQ
---

Who accepts / rejects ADR?
   The decision body is the Architecture Board until
   a new organizational structure is in place (Technical Committee).

Who can propose ADR?
   ADR are proposed by one or more Work Packages. The convener is
   the Work Package Owner.

How can I find the ADR?
   Starting with the `proposed` status, ADR are versioned in the git
   repository. Proposed ADR are communicated for comments within them
   relevant community.

When is an ADR necessary?
   First, ADR are a tool to decisions with a big impact on the architecture
   should be stated as an ADR. Furthermore, decisions with an impact on several
   Work Packages are good candidates for an ADR.-