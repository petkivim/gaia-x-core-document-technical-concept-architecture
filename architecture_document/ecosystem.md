# Gaia-X Ecosystems

## Gaia-X as Enabler for Ecosystems

The Gaia-X Architecture enables Ecosystems and data spaces using the
elements explained in the [Gaia-X Conceptual Model](conceptual_model.md) in general
and the [Federation Services](conceptual_model.md#federation-services) in particular.

An Ecosystem is an organizing principle describing the interaction of
different actors and their environment as an integrated whole, like in a
biological Ecosystem. In a technical context, it refers to a set of
loosely coupled actors who jointly create an economic community.

Gaia-X proposes a structuring into a data Ecosystem and an
Infrastructure Ecosystem, each with a different focus on exchanged goods
and services. Despite each of them has a separate focus, they cannot be
viewed separately as they build upon each other.

The Gaia-X Ecosystem consists of the entirety of all individual
Ecosystems that use the Architecture and conform to Gaia-X requirements.
Several individual Ecosystems may exist (e.g. Catena-X) that orchestrate
themselves, use the Architecture and may or may not use the Federation
Services open source software.

![](media/image11.png)
*Gaia-X Ecosystem Visualization*

The basic roles of Consumer and Provider are visualized as different
squares, while the Federator appears as connecting element. Federation
Services are presented as connection between the different elements as
well as between the different Ecosystems. The star-shaped element
visualizes that Consumers can act also as Providers by offering composed
services or processed data again via Catalogues. Governance comprises
the Policy Rules, which are statements of objectives, rules, practices
or regulations governing the activities of Participants within the
Ecosystem. Additionally, the Architecture of Standards defines a target
for Gaia-X by analysing and integrating already existing standards for
data, sovereignty and infrastructure components.

## The Role of Federation Services for Ecosystems

The following igure visualizes how the Federation Service Instances
are related to the Federator described in the conceptual model (see
section [Federator](conceptual_model.md#federator)). The Federators enable the Federation Services by
obliging Federation Service Providers, which provide the concrete
Federation Service Instances. The sum of all Federation Service
Instances form the Federation Services.

![](media/image12.png)
*Federation Services Relations*

### Goals of Federation Services

Federation Services aim to enable and facilitate interoperability and
portability of Assets and Resources within Gaia-X-based Ecosystems and
to provide Data Sovereignty. They ensure trust between Participants,
make Assets and Resources searchable, discoverable and consumable, and
provide means for Data Sovereignty in a distributed Ecosystem
environment.

They do not interfere with the business models of other members in the
Gaia-X Ecosystem, especially Providers and Consumers. Federation
Services are centrally defined while being federated themselves, so that
they are set up in a federated manner. In this way, they can be used
within individual Ecosystems and communities and, through their
federation, enable the sharing of data and services across Ecosystems or
communities as well as the interoperability and portability of data. The
set of Ecosystems that use the Federation Services form the Ecosystem.

### Nesting and Cascading of Federation Services

Federation Services can be nested and cascaded. Cascading is needed, for example, to ensure uniqueness of identities and Catalogue entries across different individual Ecosystems / communities that use the Federation Services. (Comparable to DNS servers: there are local servers, but information can be pushed up to the root servers).

Therefore, a top-level Federation Service is necessary, which will be described in detail in the upcoming Gaia-X Federation Services documents.

### Ecosystem Governance vs. Management Operations

To enable interoperability, portability and Data Sovereignty across
different Ecosystems and communities, Federation Services need to adhere
to common standards. These standards (e.g. related to service
self-description, digital identities, logging of data sharing
transactions, etc.) must be unambiguous and are therefore defined by the
Gaia-X association AISBL. The Gaia-X association AISBL owns the Compliance framework and related
regulations or governance aspects. Different entities may take on the
role of Federator and Federation Services Provider.

#### Avoiding Silos

There may be Ecosystems that use the open source Federation Services but do not go through the Compliance and testing required by the Gaia-X AISBL. This does not affect the functionality of the Federation Services within the respective Ecosystem but would hinder their interaction.

To enable open Ecosystems and avoid "siloed" use of Federation Services, only those that are compliant, interoperable (and tested) are designated as Ecosystems. Therefore, the Federation Services act as a connecting element not only between different Participants, commodities, but also Ecosystems (see above).

The following table presents how the Federation Services contribute to
the Architecture Requirements that are mentioned in section [Architecture Requirements](overview.md#architecture-requirements).


| Requirement                       | Relation to the Federation Services |
|-----------------------------------|-------------------------------------|
| Interoperability    | <ul><li>The Federated Catalogues ensure Providers to offer services along the whole technology stack. The common Self-Description scheme also enables interoperability.</li><li>A shared Compliance framework and the use of existing standards supports the combination and interaction between different Assets & Resources.</li><li>The Identity and Trust mechanisms enable unique identification in a federated, distributed setting.</li><li>The possibility to exchange data with full control and enforcement of policies as well as logging options encourages Participants to do so. The semantic interoperability enables that data exchange.</li></ul> |
| Portability         | <ul><li>The Federated Catalogues encourage Providers to offer Assets and Resources with transparent Self-Descriptions and make it possible to find the right kind of service that fits exactly and makes the interaction possible.</li><li>The open source implementations of the Federation Services provide a common technical basis and enables to move Assets and Resource in ecosystems and between different ones.</li><li>Common compliance levels and the re-use of existing standards supports portability of data and services.</li></ul> |
| Sovereignty         | <ul><li>Identity and Trust provide the foundation for privacy considerations as well as access and usage rights. Standards for sovereign data exchange enable logging functions and Usage Policies. The Self-Descriptions offer the opportunity to specify and attach Usage Policies for Data Assets.</li></ul>|
| Security and trust  | <ul><li>The Architecture and Federation Services provide definitions for trust mechanisms that can be enabled by different entities and enable transparency.</li><li>Sovereign Data Exchange, as well as Compliance concerns address security considerations. The identity and trust mechanisms provide the basis. The Federated Catalogues present Self-Descriptions and provide transparency over Service Offerings.</li></ul> |

*Federation Services match the Architecture Requirements*


### Infrastructure Ecosystem

The Infrastructure Ecosystem has a focus on computing, storage and
Interconnection elements. Speaking in terms of Assets and Resources,
these elements are addressed as Nodes, Interconnection and different
Software Assets. They range from low-level services like bare metal
computing up to high sophisticated offerings, such as high-performance
computing. Interconnection services ensure secure and performant data
exchange between the different Providers, Consumers and their services.
Gaia-X enables combinations of services that range across multiple
Providers of the Ecosystem.

### Data Ecosystem

Gaia-X facilitates Data Spaces which present a virtual data integration
concept, where data are made available in a decentralised manner, for
example, to combine and share data of different cloud storages. Data
Spaces form the foundation of Data Ecosystems. In general, Data
Ecosystems enable Participants to leverage data as a strategic resource
in an inter-organizational network without restrictions on a fixed
defined partner or central keystone companies. For data to unfold its
full potential, it must be made available in cross-company,
cross-industry Ecosystems. Therefore, Data Ecosystem not only enables
the whole data value chain but provides technical means to enable Data
Sovereignty. Such sovereign data sharing addresses different layers and
enables a broad range of business models that would not be possible
otherwise. Trust and control mechanisms foster the general amount of
data sharing and proliferate the Ecosystem growth.

### Federation, Distribution, Decentralization and Sharing

The principles of federation, distribution, decentralization and sharing
are emphasized in the Federation Services as they provide several
benefits for the Ecosystem:


| Principle        | Need for Gaia-X          | Implemented in Gaia-X Architecture        |
|------------------|--------------------------|-------------------------------------------|
| Decentralization | Decentralization will ensure Gaia-X is not controlled by the few and strengthens the participation of everyone. It also adds key technological properties like redundancy, and therefore resilience against unavailability and exploitability. Different implementations of this architecture create a diverse Ecosystem that can reflect the respective requirements of its Participants.<br><br>(example: IP address assignment) | The role of Federators may be taken by diverse actors.<br><br>The open source Federation Services can be used and changed according to own requirements as long as they are compliant and tested. |
| Distribution     | Distribution fosters the usage of different Assets and Resources by different Providers spread over geographical locations.<br><br>(Example: Domain Name System) | The Self-Description allows to describe every Asset, Resource and Service Offering in a standardized way and enables to list them in a Catalogue and assign a unique Identifier. Therefore, it enables to handle the de-facto distribution of commodities. |
| Federation       | Federation technically enables connections and a web of trust between different parts of the Ecosystem. It addresses the following challenges:<ul><li>Decentralized processing locations</li><li>Multiple actors and stakeholders</li><li>Multiple technology stacks</li><li>Special policy requirements or regulated markets</li></ul><br>(Example: Autonomous Systems) | Each system can interact with each other, e.g. the Catalogues could exchange information and the Identity remains unique. Further, different Conformity Assessment Bodies may exist. |
| Sharing          | Sharing of the relevant services and components and contributes to the Ecosystem development.<br><br>Sharing of Assets and Resources across the Gaia-X Ecosystem enables spillovers and opens up various economic growth opportunities. | The Federated Catalogues enable the matching between Provider and Consumer. Sovereign Data Exchange lowers hurdles for data exchange and Ecosystem creation. |

*Summary of Federation Services as enabler*


By utilizing common specifications and standards, harmonized rules and
policies Gaia-X is aligned with specifications like NIST Cloud
Federation Reference Architecture[^25]:

-   Security and collaboration context are not owned by a single entity

-   Participants in the Gaia-X association AISBL jointly agree upon the common goals
    and governance of the Gaia-X association AISBL

-   Participants can selectively make some of their Assets and Resources
    discoverable and accessible by other Participants in compliance with
    Gaia-X

-   Providers can restrict their discovery and disclose certain
    information but might lose the Gaia-X compliance level

[^25]: Bohn, R. B., Lee, C. A., & Michel, M. (2020). The NIST Cloud Federation Reference Architecture: Special Publication (NIST SP) - 500-332. NIST Pubs. <https://doi.org/10.6028/NIST.SP.500-332>

## Interoperability and Portability for Infrastructure and Data

For the success of a federated Ecosystem it is of importance that data,
services and the underlying technology can interact seamlessly with each
other. Therefore, portability and interoperability are two key
requirements for the success of Gaia-X as they are the cornerstones for
a working platform and ensure a functional federated, multi-provider
environment.

Interoperability is defined as the ability of several systems or
services to exchange information and to use the exchanged information
mutually. Portability refers to the enablement of data transfer and
processing to increase the usefulness of data as a strategic resource.
For services, portability implies that they can be migrated from one
provider to another, while the migration should be possible without
significant changes and adaptations and have a similar QoS.

### Areas of Interoperability and Portability

The Gaia-X Ecosystem includes a huge variety of Participants and Service Offerings. Therefore, interoperability needs to be ensured on different levels (Infrastructure services, platform as a service, software as a service, data, and others).

Regarding interoperability of data, core elements to be identified in this endeavour are API specifications and best practices for semantic data descriptions. The use of semantic interoperability is seen as a foundation to eventually create a clear mapping between domain-specific approaches based on a community process and open source efforts.

## Infrastructure and Interconnection

To best accommodate the wide variety of Service Offerings, the Gaia-X
Architecture is based on the notion of a sovereign and flexible
Interconnection of networks and Data Ecosystems, where data may be
flexibly exchanged between different Participants. Therefore,
Interconnection presents a dedicated category of Assets as described in
section [Gaia-X Conceptual Model](conceptual_model.md).

There is a strong need for Interconnection of the different Nodes in
Gaia-X. It supports the federation of the Infrastructure Ecosystem,
which in turn builds the basis for the Data Ecosystem. Due to the
different needs of the Consumers and Providers as well as due to the
highly heterogeneous architectures, different requirements arise for
those Interconnections.

### The Support of Interconnection and Networking Services

A high-level overview, which outlines the needs of the use cases in
Gaia-X with respect to Interconnection and networking services is shown
in the figure below[^26]. Such a perspective enables a differentiated service
capability between "Best Effort" services, e.g., basic Internet
connectivity, and more elevated services, which can be provided by
dedicated Interconnection and networking services. Consequently, and as
explained in section [Provider Use Cases](usecase.md#provider-use-cases), the Federated Catalogues must be extended with
adequate networking and Interconnection services, considering, for
instance, functional and non-functional QoS requirements; portability
requirements, etc.

[^26]: For a comprehensive view of the current discussion in the broader Gaia-X community, extra documents from the open working packages can be found on the Gaia-X community platform at <https://gaia.coyocloud.com/web/public-link/e01b9066-3823-42a7-b10b-9596871059ef/download>.

![](media/image13.png)
*Gaia-X network requirements to use case mapping.*

Currently, Gaia-X addresses the architectural needs for networking and
Interconnection via three building blocks: (i) a Self-Description model, 
which describes Interconnection Assets and attributes necessary to describe 
networking services; (ii) inter-node measurements, describing connectivity
between GAIA-X Participants; (iii) interconnection and networking services
based on Internet and their assessment via QoS indicators.

Given these three building blocks, the focus is mainly on the
Self-Description of Gaia-X Nodes, where Interconnection and networking
are addressed via the definition of attributes. The Self-Descriptions
for the Gaia-X infrastructure currently consider QoS functional
parameters relevant for real-time data services, e.g., latency, data
rates, bandwidth. Non-functional requirements for supported services
need to be defined as well. Therefore, Self-Description of
Interconnection and networking services should not be limited to QoS but
also address quality of experience (QoE)-related attributes and consider
non-functional requirements, such as security and reliability. A
distinct and rich description of these functional and non-functional
requirements enables differentiating between the different Service
Offerings and helps to select the appropriate Interconnection and
networking service from the Federated Catalogues.

### Network Service Composition

Networking and Interconnection services can be composed via
heterogeneous offerings from multiple Providers and technologies. To
achieve flexibility but also sovereignty and trust, network service
composition shall be supported. It is also relevant to consider the
capability to describe Interconnection and networking services in a
flexible way. Such a composition must take existing approaches into
consideration and must be as rich as, e.g., composing a slice for
verticals, via private and public Clouds[^27].

[^27]: For a comprehensive view of the current discussion in the broader Gaia-X community, extra documents from the open working packages can be found on the Gaia-X community platform at <https://gaia.coyocloud.com/web/public-link/e01b9066-3823-42a7-b10b-9596871059ef/download>.

A network service composition framework embeds both functional and
non-functional requirements and has the capability to integrate metadata
(e.g., in the form of intents) to consider abstract descriptions of the
networking service components with their related requirements. Interface
definition languages need to be adopted to enable the composition of
functional elements to support network service composition. Furthermore,
taking the non-functional aspects for networking services into
consideration, the chosen interface definition languages have to be
coupled with data modelling languages. This supports the consideration
and integration of non-functional elements when composing network
services.

In addition to non-constraining interface definition languages and data
modelling languages, an overall networking service description framework
needs to be used. Examples of available service description frameworks
that are relevant to consider in Gaia-X are, for instance, the OASIS
Topology and Orchestration Specification for Cloud Applications
(TOSCA)[^28]. With respect to network service management and
orchestration, potential candidates cover but are not limited to the ONF
Software Defined Network (SDN) architecture and the ETSI Standards for
Network Function Virtualization (ETSI NFV)[^29].

[^28]: OASIS (2013). Topology and Orchestration Specification for Cloud Applications Version 1.0. <http://docs.oasis-open.org/tosca/TOSCA/v1.0/TOSCA-v1.0.html>
[^29]: ETSI. Network Functions Virtualisation (NFV). <https://www.etsi.org/technologies/nfv>

A crucial aspect to achieve an adequate network service composition is
to integrate support for the intertwining of networking services and
application level services. Thus, both semantic and syntactic
interoperability need to be ensured. Specifically, an adequate and
semantic support for the available and multiple communication protocols
is required. This relates to the OSI Layer 2 and 3 communication
aspects, but it has also to accommodate additional protocols. Each use
case has its own set of building blocks. Therefore, the Interconnection
services should cover diverse scenarios ranging from a single
point-to-point connection to complex multipoint architectures. For
example, the open IX-API[^30] as well as solutions from the area of
Software Defined Networking can be used to flexibly interconnect and
configure these architectures, and consider host-reachability and
content-oriented developments.

[^30]: IX-API. IX-API. <https://ix-api.net/>

One further important aspect is that Interconnection services need to be
composed according to customers' requirements and applications being
served. Semantic and syntactic interoperability, as stated previously,
need therefore to be addressed also by ensuring that the described
networking and Interconnection services can be adequately associated
with Self-Descriptions, offered as Gaia-X services, so that they can be
looked up in the Federated Catalogues and can be used in composing more
complex services by Gaia-X users[^31].

In order to ensure certain requirements of latency, bandwidth and security Gaia-X has to be able to propose more than the classic Internet with the Best-Effort principle does. The solution that we see is the Gaia-X Interconnection platform - a common or standardized API via which the interconnected WANs can exchange and make their services available. IX-API could be a possible solution to implement such a platform. This service will be provided and operated by Gaia-X Provider (e.g. Interconnection Provider).

The resources and data from the Provider and User use cases are located in different physical locations, namely in data centers that could be spread all over Europe. If we would interconnect them directly that would result in the redundant number of connections, which would be expensive, insufficient and neither dynamic nor fast process. In the example shown below, if we want to interconnect 8 data centers we would require 28 connections (picture on the left). However, with the introduction of the Interconnection platform we would need only eight connections (picture on the right). This not only means that multi-cloud setups will become easier and faster, moreover dynamic service provisioning will be possible. It will ensure the competitiveness of Gaia-X against the existing hyperscalers, as the elevated networking and interconnection services will be met.

For those customers who do not want that their traffic runs via the platform it is possible to create for example Closed User Group (multipoint VPN) or a private point-to-point connection. 

![](media/Gaia-X_interc.png)

[^31]: For a comprehensive view of the current discussion in the broader Gaia-X community, extra documents from the open working packages can be found on the Gaia-X community platform at <https://gaia.coyocloud.com/web/public-link/e01b9066-3823-42a7-b10b-9596871059ef/download>.
