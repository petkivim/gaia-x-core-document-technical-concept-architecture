## Data Logging Service

The Data Logging Service is a Federation Service of the category Data Sovereignty Service and provides log messages to trace relevant information about the data exchange transaction.

### alias
- DLS

### references
- Federation Services Specification GXFS
