## Federated Trust Component

A [Federation Service](conceptual_model.md#federation-services) component, which ensures trust and trustworthiness between GAIA-X and the interacting [Identity System](#identity-system) of the [Participant](#participant).

This component guarantees identity proofing of the involved Participants to make sure that GAIA-X Participants are who they claim to be.

### alias
Federated Trust Model

