## Federator

Federators are in charge of the [Federation Services](#federation-services) and the [Federation](#federation) which are autonomous of each other.

Federators are GAIA-X [Participants](#participant).

There can be one or more Federators per type of Federation Service.
