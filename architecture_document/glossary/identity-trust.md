## Identity and Trust

Identity and Trust is a [Gaia-X Federation Service](#federation-services).

It ensures [Participants](#participant) in a Gaia-X Ecosystem are who they claim to be and enables identity and access management for [Providers](#provider) and [Consumers](#consumer).
