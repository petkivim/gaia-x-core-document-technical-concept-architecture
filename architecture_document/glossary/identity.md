## Identity

An Identity is a representation of an entity ([Participant](#participant)/[Asset](#asset)/[Resource](#resource)) in the form of one or more attributes that allow the entity to be sufficiently distinguished within context.

An identity may have several Identifiers.

### references
- ITU-T Recommendation X1252, Baseline identity management terms and definitions
