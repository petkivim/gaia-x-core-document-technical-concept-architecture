## Information Rights Management

Information Rights Management (IRM) is a sub-type of Digital Rights Management (DRM) used (as one option) for the protection of enterprise data and to ensure usage only by authorised parties and only according to agreed license terms.

In GAIA-X this could include technology to restrict access to users within the EU or another jurisdiction after the data has been delivered.

Due to cost and complexity, IRM is most likely to be used only on the most valuable or sensitive of shared data, or where liability could arise from misuse by the recipient.
