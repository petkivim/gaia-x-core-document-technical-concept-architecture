## Interconnection

An Interconnection is a connection between two or multiple [Nodes](#node).

These nodes are usually deployed in different infrastructure domains and owned by different stakeholders, such as customers and/or providers.

The Interconnection between the nodes can be seen as a path which exhibits special characteristics, such as latency and bandwidth guarantees, that go beyond the characteristics of a path over the public Internet.
