## Provider Access Management (Provider AM)

The Service Ordering Process will involve the [Consumer](#consumer) and the [Provider](#provider).

This component is internal to the [Provider](#provider).

The Service Provider will create the Service Instance and will grant access for the [Consumer](#consumer) by this component.

### references
- AM Framework Document and Technical Architecture Paper R. June 2020
