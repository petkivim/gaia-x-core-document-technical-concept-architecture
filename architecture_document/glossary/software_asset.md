## Software Asset

Software Assets are a form of [Assets](#asset) that consist of non-physical functions, like source-code.

A running instance of a Software Asset has a PID and is considered to be a [Resource](#resource).

### references
- PID: <https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_300>
