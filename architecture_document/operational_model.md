# Gaia-X Operating Model

Gaia-X in its unique endeavor must have an operating model enabling a wide adoption from small and medium-sized enterprises up to large organisations, including highly regulated markets, to be sustainable and scalable.

To achieve the above statements, here is a non-exhaustive list of Critical Success Factors (CSF)

1. The solution must provide clear, undoubtedly, added values to the Participants
2. The solution must have a transparent governing model with identified accountability and liability.
3. The solution must be easy to use by its Participants
4. The solution must be financially sustainable for the Participants and the Gaia-X association.

## Decentralized Autonomous Organization

Decentralized Autonomous Organization[^dao], DAO, is a type of governing model where:

- There is no central leadership.
- Decision are made by the community's members.
- The regulation is done by a set of automatically enforceable rules on a public blockchain whose goal is to incentive its community's members to achieve a shared common mission.
- The organization has its own rules, including for managing its own funds.

[^dao]: <https://blockchainhub.net/dao-decentralized-autonomous-organization/>

## Gaia-X association roles

Based on the objective and constrains to achieve those objectives, Gaia-X association is creating a Gaia-X DAO.

The Gaia-X association, GAIA-X European Association for Data and Cloud, AISBL, has few roles:

1. Provide a decentralized network with smart contract functionality, a Gaia-X blockchain
2. Provide a Gaia-X token enabling transactions on the decentralized network. The token will be based on an EUR stablecoin[^stableeur].
3. Provide the minimal set of rules to implement interoperability across Participants and Ecosystems.
Those rules include how to validate Self-Descriptions, how to issue Labels, how to discover and register on the Gaia-X network other community's Ecosystems and Participants. The rules will be enforced via smart contracts and oracles[^oracles].
4. Provide and operate a decentralized catalogue[^OP].
5. Provide and maintain a list of Self-description URI violating Gaia-X membership rules. This list must be used by all Gaia-X Catalogue providers to filter out inappropriate content.

[^stableeur]: example are `EURS` and `sEUR`.
[^oracles]: Various oracles can be implemented, include decentralized ones with <https://chain.link/>
[^OP]: Based on the technology of <https://oceanprotocol.com/>

## Decentralized Consensus algorithms

There is a wide range of existing consensus algorithms[^proofofx]. Among the top, we have `Proof of Work` used for exemple by Bitcoin. It's very energy consuming. We have `Proof of Stake` used by Etherum which require a minimal set of nodes to be applicable.

Finally, and for Gaia-X, the proposal is to use `Proof of Authority`, with each Board of Director member operating a blockchain node. This is however not a final decision and open for improvement with newer algorithms such as `Proof of Participation`[^PoP].

[^proofofx]: [Study of Blockchain Based Decentralized Consensus Algorithms - DOI 10.1109/TENCON.2019.8929439](http://scis.scichina.com/en/2021/121101.pdf)
[^PoP]: <https://zoobc.how/?qa=92/what-is-proof-of-participation-in-simple-words>

## Interoperability rules

Gaia-X participants which agree to a specific set of additional rules or scope may constitute Gaia-X ecosystem instances with their own governance (e.g. Catena-X, Datenraum Mobility, ...).

Those ecosystem instances can register themselves on the Gaia-X network as Gaia-X Ecosystems under the strict condition that they provide a Label assessing of their interoperability level.

### Interoperability criteria

| Criteria | Mandatory | Rules                                                |
|----------|-----------|------------------------------------------------------|
| Base     | Yes       | <ul><li>Must use Gaia-X Self-description format and ontology</li><li>Must provide public access to its raw Self-Description files</li><li>Must use Verifiable credentials</li></ul> |
| Infra    | No        | Must use Gaia-X compliant composition format         |
| Data     | No        | Must use Gaia-X compliant data description format    |
| Identity | No        | Must use Gaia-X compliant Identity methods           |
| Access   | No        | Must use Gaia-X compliant digital rights and access descriptions |
| Usage    | No        | Must use Gaia-X compliant usage policy description[^drm] |

[^drm]: Could be enforced by Digital Right Management and <https://www.openpolicyagent.org/> or similar.  

Using this mechanism, a `Provider` from one Ecosystem can have their `Service Offering` made available to others Ecosystems. Other Ecosystems may include those `Service Offering`s in their catalogues depending on their internal rules and the interoperability level.

Not all interoperability levels are mandatory to enable the inclusion of existing ecosystems. More mandatory levels may be enforced as the Gaia-X DAO updates its rules.

#### Base layer

The base layer enables to have multiple signatures on a single Self-Description.

## Decentralized Catalogue

Gaia-X association providing a decentralized catalogue will enable a fast and transparent access to service providers and services, including Infrastructure, Data and Algorithms offers.

Any Ecosystem can extend this catalogue by registering their own Self-description storage URI in the Gaia-X blockchain.
This creates a `Catalogue of Catalogues` or more precisely a `ledger of Self-description directories`.

Any Ecosystem can create their specific catalogue out of the decentralized published Self-Descriptions.

### Data curation

By offering transparent access to structured and verifiable service descriptions, plus visibility on Service consumption, the Participants can extrapolate about the data quality.

Other metadata, such as using [Great Expectations](https://github.com/great-expectations/great_expectations) can be enforced at the Data interoperability layer to promote a Data quality score.
