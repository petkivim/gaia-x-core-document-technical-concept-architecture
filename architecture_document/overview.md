# Overview

## Introduction 

Gaia-X aims to create a federated open data infrastructure based on
European values regarding data and cloud sovereignty. The mission of
Gaia-X is to design and implement a data sharing architecture that
consists of common standards for data sharing, best practices, tools,
and governance mechanisms. It also constitutes an EU federation of cloud
infrastructure and data services, to which all 27 EU member states have
committed themselves[^1]. This overall mission drives the Gaia-X
Architecture.[^2]

[^1]: European Commission. (2020). Towards a next generation cloud for Europe. <https://ec.europa.eu/digital-single-market/en/news/towards-next-generation-cloud-europe>

[^2]: Federal Ministry for Economic Affairs and Energy. (2019). Project Gaia-X: A Federated Data Infrastructure as the Cradle of a Vibrant European Ecosystem.
<https://www.bmwi.de/Redaktion/EN/Publikationen/Digitale-Welt/project-Gaia-X.htm>

The Gaia-X Architecture identifies and describes the concepts of the
targeted federated open data infrastructure as well as the relationships
between them. It describes how Gaia-X facilitates interoperability and
interconnection between all participants in the European digital
economy, with regard to both data and services.

This draft for the Gaia-X Architecture addresses stakeholders from
industry, the public sector, science and other stakeholders. It replaces
the former architecture document Gaia-X: Technical Architecture, Release
-- June 2020.[^3].

[^3]: Federal Ministry for Economic Affairs and Energy. (2020). Gaia-X: Technical Architecture: Release - June, 2020.

## Objectives

This document describes the top-level Gaia-X Architecture model. It
focuses on conceptual modelling and is agnostic regarding technology and
vendor. In doing so, it aims at representing the unambiguous
understanding of the various Gaia-X stakeholder groups about the
fundamental concepts and terms of the Gaia-X Architecture in a
consistent form at a certain point in time.

It forms the foundation for further elaboration, specification, and
implementation of the Gaia-X Architecture. Thus, it creates an
authoritative reference for the Gaia-X Federation Services
specification.

The Gaia-X Architecture Document is subject to continuous updates
reflecting the evolution of business requirements (e.g. from dataspace
activities in Europe), relevant changes in regulatory frameworks, and
the advancements regarding the technological state of the art.

## Scope

The Gaia-X Architecture document describes the concepts required to set
up the Gaia-X Data and Infrastructure Ecosystem. It integrates the
Providers, Consumers, and Services needed for this interaction. These
Services comprise ensuring identities, implementing trust mechanisms,
and providing usage control over data exchange and Compliance -- without
the need for individual agreements.

The Gaia-X Architecture Document describes both the static decomposition
and dynamic behaviour of the Gaia-X core concepts and Federation
Services.

Details about implementing the Gaia-X Ecosystem are to be defined
elsewhere (see "Architecture of Standards").

At present, automated contracts, legal binding, monitoring, metering as
well as billing mechanisms, amongst others, are not within the scope of
this document.

The Gaia-X Architecture document includes a glossary which identifies
and defines those terms that have a distinct meaning in Gaia-X, which
may slightly deviate from everyday language, or have different meanings
in other architectures or standards.

## Audience and Use

The Gaia-X Architecture document is directed towards all Gaia-X
interests and stakeholder groups, such as Gaia-X association AISBL members, Hub
participants, and employees of companies or individuals interested in
learning about the conceptual foundation of Gaia-X.

It should be used as an entry point to get familiar with the fundamental
concepts of Gaia-X and their relationship between each other and as a
reference for elaboration and specification of the Gaia-X Architecture.

## Relation to other Gaia-X Documents

The present document is prepared by the Working Group "Architecture"
within the Technical Committee, of which roles and responsibilities will
be documented in the Operational Handbook (yet to be published).
Additional Compliance-relevant information will be outlined in the
documents on "Policies & Rules" as well as "Architecture of Standards".
The Federation Services specification, which is also the basis for the
upcoming open source implementation adds details the Federation Service
functionalities as well as the upcoming test bench.

![](media/image1.png)
*Relation to other Documents*

## Architecture Governance and next Steps

The Gaia-X Architecture document contains contributions from various
Gaia-X Working Groups. It is the linking pin to the associated
artefacts, providing the top-level conceptual model definitions that are
the basis for further specification and implementation. Changes (Request
for Change or Errors) are managed in the Architecture Decision Record
(ADR) process documented in a collaboration tool[^4]. A more elaborated
version of this Gaia-X Architecture document will be released in June
2021. This will allow contributions from all Gaia-X members.

[^4]: Gaia-X association AISBL. Architecture Decision Record (ADR) Process: GitLab Wiki. <https://gitlab.com/Gaia-X/Gaia-X-technical-committee/Gaia-X-core-document-technical-concept-architecture/-/wikis/home>

## Architecture Requirements 

The architecture is utilized to address the following requirements:

- **Interoperability of data and services**: The ability of several
  systems or services to exchange information and to use the exchanged
  information mutually.

- **Portability of data and services:** Data is described in a
  standardized protocol that enables transfer and processing to
  increase its usefulness as a strategic resource. Services can be
  migrated without significant changes and adaptations and have a
  similar quality of service (QoS) as well as the same Compliance
  level.

- **Sovereignty over data:** Participants can retain absolute control
  and transparency over what happens to their data. This document
follows EU's data protection provisions and emphasizes a general 'compliance-by-design' and 'continuous-auditability' approach.

- **Security and trust:** Gaia-X puts security technology at its core
  to protect every Participant and system of the Gaia-X Ecosystem
  (security-by-design). An Identity management system with mutual
  authentication, selective disclosure, and revocation of trust is
  needed to foster a secure digital Ecosystem without building upon
  the authority of a single corporation or government.

This architecture describes the technical means to achieve that, while
being agnostic to technology and vendors.

## Architecture Design Principles

The following design principles[^5] underlie the architecture:

_Name - Statement - Rationale - Implications_

- **Federation:**
 Federated systems describe autonomous entities, tied together by
 a specified set of standards, frameworks, and legal rules.
 The principle balances the need for a minimal set of requirements to enable
 interoperability and information sharing between the different entities
 while giving as high autonomy as possible to them.
 The principle defines the orchestrating role of Gaia-X governance elements
 and implies interoperability within and across Gaia-X Ecosystems.


- **Decentralization:** 
 Decentralization describes how lower-level entities operate locally
 without centralized control in a self-organized manner.
 (The federation principle enables this self-organization by providing capabilities for connectivity
 within a network of autonomous acting Gaia-X Participants.)
 The principle of decentralization implies individual responsibility for contributions
 and no control over the components, which fosters scalability.


- **Openness**:
 The open architecture makes adding, updating, and changes of components easy 
 and allows insights into all parts of the architecture without any proprietary claims.
 This way Gaia-X is open to future innovation and standards and aware about evolving technologies.
 The documentation and specifications of Gaia-X architectures and technologies are openly available 
 and provide transparency as technology choices will be made to encourage the distribution of collaboratively created artifacts
 under OSD[^6] compliant open source licenses[^7].

[^5]: TOGAF 9.2. Components of Architecture Principles. <https://pubs.opengroup.org/architecture/togaf9-doc/arch/chap20.html#:~:text=Architecture%20Principles%20define%20the%20underlying,for%20making%20future%20IT%20decisions>.
[^6]: Open Source Initiative. The Open Source Definition (Annotated). <https://opensource.org/osd-annotated>
[^7]: Open Source Initiative. Licenses & Standards. <https://opensource.org/licenses>
