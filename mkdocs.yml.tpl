site_name: GaiaX Architecture Document -June 2021 - DRAFT version ${CI_COMMIT_SHORT_SHA}
theme: readthedocs
repo_url: https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/
edit_uri: edit/master/architecture_document/
docs_dir: architecture_document
site_dir: public
extra_css: [extra.css]

nav:
 - README.md
 - overview.md
 - conceptual_model.md
 - operational_model.md
 - federation_service.md
 - usecase.md
 - ecosystem.md
 - glossary.md
 - reference.md
 - appendix.md

plugins:
 - search
 - mermaid2
 - exclude:
    glob:
    - glossary/*
 - with-pdf:
    verbose: true
    author: Gaia-X, European Association for Data and Cloud, AISBL
    copyright: ©2021 Gaia-X, European Association for Data and Cloud, AISBL
    cover_title: Gaia-X Architecture Document
    cover_subtitle: June 2021 - DRAFT version ${CI_COMMIT_SHORT_SHA}
    render_js: true
    headless_chrome_path: chromium
    debug_html: true

markdown_extensions:
 - smarty
 - toc
 - footnotes
